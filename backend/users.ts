export class User {
    constructor(public email: string,
                public name: string,
                private password: string) {}

    matches(user: User): boolean {
        return user !== undefined &&
            user.email === this.email && 
            user.password === this.password
    }
}

export const users: {[key:string]: User} = {
    "rafaeldecasstro@gmail.com":  new User('rafaeldecasstro@gmail.com', 'Rafael de Castro', 'rafael30'),
}
