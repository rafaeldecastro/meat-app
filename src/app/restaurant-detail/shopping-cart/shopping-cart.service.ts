import { Injectable } from '@angular/core';

import { CartItem } from "./cart-item.model";
import { MenuItem } from "../menu-item/menuitem.model";
import { NotificationService } from 'app/shared/messages/notification.service';

@Injectable()
export class ShoppingCartService {

    public items: CartItem[] = [];

    constructor(private notificartionService: NotificationService) {}

    clear(): void {
        this.items = [];
    }

    addItem(item: MenuItem): void {
        let foundItem = this.items
            .find((cItem) => cItem.menuItem.id === item.id);
            
        if (foundItem) {
            this.increaseQty(foundItem);
        } else {
            this.items.push(new CartItem(item));
        }

        this.notificartionService.notify(`Você adicionou o item ${item.name}`);
    }

    removeItem(item: CartItem): void {
        this.items.splice((this.items.indexOf(item)), 1);
        this.notificartionService.notify(`Você removeu o item ${item.menuItem.name}`);
    }

    increaseQty(item: CartItem): void {
        item.quantity += 1;
    }

    decreaseQty(item: CartItem): void {
        item.quantity -= 1;
        if (item.quantity === 0) {
            this.removeItem(item);
        }
    }

    total(): number {
        return this.items
            .map((item) => item.value())
            .reduce((prev, value) => prev + value, 0);
    }
}