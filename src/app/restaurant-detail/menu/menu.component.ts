import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantsService } from 'app/restaurants/restaurants.service';
import { Observable } from 'rxjs';
import { MenuItem } from '../menu-item/menuitem.model';

@Component({
  selector: 'mt-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {
  $menu: Observable<MenuItem[]>;

  constructor(private restaurantsService: RestaurantsService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.$menu = this.restaurantsService.menuOfRestaurants(
      this.route.parent.snapshot.paramMap.get('id')
    );
  }

  addMenuItem(menuItem: MenuItem) {
    console.log(menuItem);
  }
}
