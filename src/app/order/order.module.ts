import { NgModule } from '@angular/core';

import { DeliveryCostsComponent } from './delivery-costs/delivery-costs.component';
import { OrderItemsComponent } from './order-items/order-items.component';
import { OrderComponent } from './order.component';

import { SharedModule } from 'app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { LeaveOrderGuard } from './leave-order.guard';

const ROUTE: Routes = [{ path: '', component: OrderComponent, canDeactivate: [LeaveOrderGuard] }];

@NgModule({
  declarations: [OrderComponent, OrderItemsComponent, DeliveryCostsComponent],
  imports: [SharedModule, RouterModule.forChild(ROUTE)]
})
export class OrderModule {}
